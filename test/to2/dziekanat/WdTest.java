package to2.dziekanat;

import com.vaadin.testbench.ElementQuery;
import com.vaadin.testbench.ScreenshotOnFailureRule;
import com.vaadin.testbench.TestBenchTestCase;
import com.vaadin.testbench.elements.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertEquals;

public class WdTest extends TestBenchTestCase {
    @Rule
    public ScreenshotOnFailureRule screenshotOnFailureRule =
            new ScreenshotOnFailureRule(this, true);

    @Before
    public void setUp() throws Exception {
        setDriver(new FirefoxDriver()); // Firefox
    }

    private void openTestUrl() {
        getDriver().get("http://localhost:8080/wd");
    }

    @Test
    public void incorrectSessionTest() throws Exception {
        openTestUrl();

        //fill form with incorrect data
        TextFieldElement textfieldIndex = $(TextFieldElement.class).caption("Indeks").first();
        textfieldIndex.sendKeys("666");
        TextFieldElement textfieldPassword = $(TextFieldElement.class).caption("Haslo").first();
        textfieldPassword.sendKeys("1234");

        //click button
        ButtonElement buttonLogin = $(ButtonElement.class).first();
        buttonLogin.click();

        //check if notification about error shown
        NotificationElement notification = $(NotificationElement.class).first();
        assertEquals("Niepoprawne dane", notification.getText());
    }

    @Test
    public void correctSessionTest() throws Exception {
        openTestUrl();

        //fill form with correct data
        TextFieldElement textfieldIndex = $(TextFieldElement.class).caption("Indeks").first();
        textfieldIndex.sendKeys("666");
        TextFieldElement textfieldPassword = $(TextFieldElement.class).caption("Haslo").first();
        textfieldPassword.sendKeys("123");

        //click button
        ButtonElement buttonLogin = $(ButtonElement.class).first();
        buttonLogin.click();

        //navigate to personal data view
        ElementQuery<HorizontalLayoutElement> horizontalLayout1 = $(HorizontalLayoutElement.class);
        ElementQuery<VerticalLayoutElement> verticalLayout1 = $(VerticalLayoutElement.class).in(horizontalLayout1);
        VerticalLayoutElement verticalLayout2 = $(VerticalLayoutElement.class).in(verticalLayout1).get(2);
        verticalLayout2.click();

        //check if name and surname correct
        TextFieldElement textfieldName = $(TextFieldElement.class).get(2);
        TextFieldElement textfieldSurname = $(TextFieldElement.class).get(3);
        assertEquals("Kuba", textfieldName.getValue());
        assertEquals("Baranski", textfieldSurname.getValue());
    }
}