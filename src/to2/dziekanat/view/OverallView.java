package to2.dziekanat.view;

import com.google.gwt.thirdparty.guava.common.collect.ImmutableMap;
import com.vaadin.ui.*;
import to2.dziekanat.WdUI;
import to2.dziekanat.util.GUI;
import to2.dziekanat.util.Style;
import to2.dziekanat.view.login.LogInView;
import to2.dziekanat.view.marks.MarksView;
import to2.dziekanat.view.personal.PersonalDataView;

public class OverallView extends VerticalLayout {

    public static final String ANNOUNCEMENTS = "Ogloszenia";
    public static final String MARKS = "Oceny";
    public static final String PERSONAL_DATA = "Dane osobowe";
    public static final String NOTIFICATIONS = "Zgloszenia";

    private final ImmutableMap<String, Choosable> viewMap = ImmutableMap.<String, Choosable>builder()
            .put(ANNOUNCEMENTS, new AnnouncementsView())
            .put(MARKS, new MarksView())
            .put(PERSONAL_DATA, new PersonalDataView())
            .put(NOTIFICATIONS, new NotificationsView()).build();

    private Choosable currentComponent = viewMap.get(ANNOUNCEMENTS);
    private VerticalLayout highlightedMenuItem;
    private Layout currentComponentHolder = GUI.horizontal().margin().get();

    public OverallView() {
        currentComponentHolder.addComponent(currentComponent);
        HorizontalLayout layout = GUI.horizontal().margin().styles("mainLayout")
                .addComponent(createMenu())
                .addComponent(currentComponentHolder).get();
        Button logoutButton = GUI.button().onClick(event -> logout()).caption("Wyloguj").sizeUndefined().get();
       /* Label user = GUI.label().value("Zalogowany jako: " + UI.getCurrent().getSession().getAttribute("user") + " ")
                .sizeUndefined().get();*/
        addComponent(GUI.horizontal().styles("topComponent").margin()
                //.addComponent(user).align(user, Alignment.BOTTOM_RIGHT)
                .addComponent(logoutButton).align(logoutButton, Alignment.BOTTOM_RIGHT)
                .get());
        addComponent(layout);
    }

    private void logout() {
        UI.getCurrent().getSession().setAttribute("user", null);
        UI.getCurrent().setContent(new LogInView((WdUI) UI.getCurrent()));
    }

    private VerticalLayout createMenu() {
        VerticalLayout menuLayout = GUI.vertical().styles("mainMenu").get();
        for (String caption : new String[]{ANNOUNCEMENTS, MARKS, PERSONAL_DATA, NOTIFICATIONS}) {
            Label label = GUI.label().caption(caption).get();
            VerticalLayout menuItem = GUI.vertical()
                    .addComponent(label)
                    .align(label, Alignment.MIDDLE_LEFT)
                    .styles("mainMenuItem")
                    .width("200px")
                    .get();
            menuItem.addLayoutClickListener(event -> {
                currentComponent.handleMenuSelectionChange();
                currentComponentHolder.replaceComponent(currentComponent, viewMap.get(caption));
                viewMap.get(caption).handleMenuSelectionChange();
                highlightedMenuItem.removeStyleName(Style.LIGHT_GRAY);
                currentComponent = viewMap.get(caption);
                highlightedMenuItem = menuItem;
                highlightedMenuItem.addStyleName(Style.LIGHT_GRAY);
            });
            menuLayout.addComponent(menuItem);
            if (highlightedMenuItem == null) {
                highlightedMenuItem = menuItem; // initialize with Announcements being highlighted
                highlightedMenuItem.addStyleName(Style.LIGHT_GRAY);
            }
        }
        return menuLayout;
    }


}
