package to2.dziekanat.view.personal;

import com.google.common.collect.Maps;
import com.vaadin.ui.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import to2.dziekanat.model.Student;
import to2.dziekanat.model.StudentGroup;
import to2.dziekanat.persistence.HibernateUtils;
import to2.dziekanat.util.GUI;
import to2.dziekanat.view.Choosable;

import java.util.Map;
import java.util.stream.Collectors;

public class PersonalDataView extends Choosable {

    private static final String NAME = "Imie";
    private static final String INDEX = "Indeks";
    private static final String SURNAME = "Nazwisko";
    private static final String GROUPS = "Grupy";
    private static final String YEAR = "Rok";

    private Map<String, TextField> fields = Maps.newHashMap();
    private final Button editButton, saveButton;
    private Student user;

    public PersonalDataView() {
        editButton = GUI.button().caption("Edytuj").onClick(event -> edit()).get();
        saveButton = GUI.button().caption("Zapisz").onClick(event -> save()).visible(false).get();
        setContent(GUI.vertical().addComponent(createContent())
                .addComponent(editButton).addComponent(saveButton).margin().padding().get());
    }

    private Component createContent() {
        //UserData.getPerson();
        VerticalLayout layout = GUI.vertical().get();

        user = (Student) UI.getCurrent().getSession().getAttribute("user");
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            createLabel(INDEX, user.getStudentIdNumber(), false, layout);
            createLabel(YEAR, String.valueOf(user.getPrimaryYear().getYear()), false, layout);
            createLabel(NAME, user.getName(), true, layout);
            createLabel(SURNAME, user.getSurname(), true, layout);
            createLabel(GROUPS, StringUtils.join(user.getAttendedGroups().stream()
                    .map(StudentGroup::getName).collect(Collectors.toList()), ","), false, layout);
        }

        return layout;
    }

    private void createLabel(String caption, String data, boolean enabled, VerticalLayout layout) {
        Label label = GUI.label().caption(caption + ":").width("150px").get();
        TextField value = GUI.textfield().value(data).readOnly().enabled(enabled).get();
        fields.put(caption, value);
        layout.addComponent(GUI.horizontal()
                .addComponent(label)
                .addComponent(value)
                .align(label, Alignment.MIDDLE_RIGHT)
                .align(value, Alignment.MIDDLE_LEFT)
                .get());
    }

    @Override
    public void handleMenuSelectionChange() {

    }

    private void edit() {
        fields.values().stream().forEach(t -> t.setReadOnly(false));
        editButton.setVisible(false);
        saveButton.setVisible(true);
    }

    private void save() {
        fields.values().stream().forEach(t -> t.setReadOnly(true));
        editButton.setVisible(true);
        saveButton.setVisible(false);

        user.setName(fields.get(NAME).getValue());
        user.setSurname(fields.get(SURNAME).getValue());
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            Transaction trans = session.beginTransaction();
            session.update(user);
            trans.commit();
        }
    }
}
