package to2.dziekanat.view;

import com.vaadin.ui.VerticalLayout;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import to2.dziekanat.model.Announcement;
import to2.dziekanat.persistence.HibernateUtils;
import to2.dziekanat.util.ExpandableComponent;
import to2.dziekanat.util.GUI;
import to2.dziekanat.util.Style;

import java.time.LocalDateTime;
import java.util.List;

public class AnnouncementsView extends Choosable {

    public AnnouncementsView() {
    	Session session = HibernateUtils.getSessionFactory().openSession();
        List<Announcement> announcements = loadAnnouncements(session);
        session.close();
        VerticalLayout layout = GUI.vertical().padding().styles(Style.DEFAULT_BGCOLOUR).get();
        for (Announcement announcement : announcements) {
            String teacher = announcement.getIssuer().getFullName();
            LocalDateTime issueDate = announcement.getIssueDate();
            String date = issueDate.getDayOfMonth() + "." + issueDate.getMonthValue() + " " + issueDate.getHour() + ":" + String.format("%02d", issueDate.getMinute());
            ExpandableComponent component = new ExpandableComponent(teacher + " (" + date + ")", GUI.label().value(announcement.getText())
                    .styles(Style.ULTRA_LIGHT_GRAY, Style.MARGIN10, Style.PADDING10).get());
            layout.addComponent(component);
        }
        setContent(layout);
    }

    private List<Announcement> loadAnnouncements(Session session) {
        return session.createCriteria(Announcement.class).add(Restrictions.ge("issueDate", LocalDateTime.now().minusDays(30))).list();
    }

    @Override
    public void handleMenuSelectionChange() {

    }
}
