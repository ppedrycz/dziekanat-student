package to2.dziekanat.view;

import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import to2.dziekanat.model.FeedbackMessage;
import to2.dziekanat.model.Student;
import to2.dziekanat.persistence.HibernateUtils;
import to2.dziekanat.util.ExpandableComponent;
import to2.dziekanat.util.GUI;
import to2.dziekanat.util.Style;

import java.time.LocalDateTime;
import java.util.List;

public class NotificationsView extends Choosable {

    public NotificationsView() {
        setContent(createContent());
    }

    private Component createContent() {
        VerticalLayout layout = GUI.vertical().padding().styles(Style.DEFAULT_BGCOLOUR).get();
        
        Student user = (Student) UI.getCurrent().getSession().getAttribute("user");

    	try (Session session = HibernateUtils.getSessionFactory().openSession()) {
    		List<FeedbackMessage> feedbacks = getFeedbacksFromUser(user, session);

            layout.addComponent(GUI.label().value("Liczba zgłoszeń: " + feedbacks.size()).get());

            for(FeedbackMessage f : feedbacks){
                LocalDateTime gradingTime = f.getTimeSent();
                String date = gradingTime.getDayOfMonth() + "/" + gradingTime.getMonthValue() + "/" + gradingTime.getYear();
                ExpandableComponent feedback = new ExpandableComponent(date + " Grade: " + f.getCommentedGrade().getScore().toPlainString(),
                        GUI.label().value(f.getContent()).styles(Style.ULTRA_LIGHT_GRAY, Style.MARGIN10, Style.PADDING10).get());
                layout.addComponent(feedback);
            }
        }
    
        return layout;
    }

    @Override
    public void handleMenuSelectionChange() {
    	setContent(createContent());
    }
    
    private List<FeedbackMessage> getFeedbacksFromUser(Student user, Session session){
    	return session.createCriteria(FeedbackMessage.class, "feedback")
    			.createAlias("feedback.commentedGrade", "commentedGrade")
    			.createAlias("commentedGrade.gradedStudent", "student")
    			.add(Restrictions.eq("student.id", user.getId())).list();
    }
}
