package to2.dziekanat.view.marks;

import com.vaadin.ui.Button;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import to2.dziekanat.model.Course;
import to2.dziekanat.model.Grade;
import to2.dziekanat.model.Semester;
import to2.dziekanat.util.GUI;

import java.time.LocalDateTime;
import java.util.List;

public class MarksTable extends Table {

    private static final String SUBJECT = "Przedmiot";
    private static final String MARK = "Ocena";
    private static final String TEACHER = "Prowadzacy";
    private static final String DATE = "Data";
    private static final String ACTION = "Zglos blad";

    public MarksTable() {
        super();
        addContainerProperty(SUBJECT, String.class, null);
        addContainerProperty(MARK, String.class, null);
        addContainerProperty(TEACHER, String.class, null);
        addContainerProperty(DATE, String.class, null);
        addContainerProperty(ACTION, Button.class, null);
        setVisibleColumns(SUBJECT, MARK, TEACHER, DATE, ACTION);
        setColumnHeaders(SUBJECT, MARK, TEACHER, DATE, ACTION);
        setWidth("700px");
        setHeightUndefined();
        setPageLength(5);
    }

    public void loadSemester(Semester semester, Course subject, Session session) {
     	//addItem(new Object[]{"as", "3/5", "asd", "11.11", createButton()}, 1);
        //addItem(new Object[]{"as", "10/10", "asd", "12.12", createButton()}, 2);
        //addItem(new Object[]{"as", "30/50", "asd", String.valueOf(getGradesFromCourseAndUser(subject, session).size()), createButton()}, 3);
        
        List<Grade> grades = getGradesFromCourseAndUser(subject, session);
        
        int i = 1;
        
        for(Grade grade : grades){
            LocalDateTime gradingTime = grade.getGradingTime();
            String date = gradingTime.getDayOfMonth() + "/" + gradingTime.getMonthValue();
            addItem(new Object[]{
                    String.valueOf(subject.getCourseTitle()),
    				String.valueOf(grade.getScore()),
                    String.valueOf(subject.getSupervisor().getFullName()),
                    date,
                    createButton(grade)}, i++);
        }
        
        setPageLength(size() > 5 ? size() : 5);
        
    }

    //todo parameter
    private Button createButton(Grade grade) {
        return GUI.button().caption("Zglos blad").onClick(event ->
                UI.getCurrent().addWindow(new NotificationWindow(grade))).get();
    }
    
    private List<Grade> getGradesFromCourseAndUser(Course subject, Session session){
    	return session.createCriteria(Grade.class, "grade")
    			.createAlias("grade.gradedSomething", "gradedSomething")
    			.createAlias("gradedSomething.masterCourseComponent", "masterCourseComponent")
    			.createAlias("masterCourseComponent.masterCourse", "masterCourse")
    			.add(Restrictions.eq("masterCourse.id", subject.getId())).list();
    }
    
}
