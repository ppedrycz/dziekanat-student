package to2.dziekanat.view.marks;

import com.vaadin.ui.*;
import org.hibernate.Session;
import org.hibernate.Transaction;
import to2.dziekanat.model.FeedbackMessage;
import to2.dziekanat.model.Grade;
import to2.dziekanat.model.Student;
import to2.dziekanat.persistence.HibernateUtils;
import to2.dziekanat.util.GUI;

public class NotificationWindow extends Window {

    private TextArea textArea;
    private Grade grade;

    public NotificationWindow(Grade grade) {
    	this.grade = grade;
        setCaption("Zglaszanie bledu");
        setWidth("400px");
        setHeightUndefined();
        setContent(createContent());
        setModal(true);
    }

    private Component createContent() {
        Button button = GUI.button().caption("Wyslij").onClick(event -> send()).get();
        return GUI.vertical().margin()
                .addComponent(textArea = GUI.textarea().sizeFull().height("200px").get())
                .addComponent(button)
                .align(button, Alignment.MIDDLE_CENTER)
                .get();
    }

    private void send() {
    	//try (Session session = HibernateUtils.getSessionFactory().openSession()) {
        Student user = (Student) UI.getCurrent().getSession().getAttribute("user");

        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction trans = session.beginTransaction();
        session.save(new FeedbackMessage(user, this.grade, textArea.getValue()));
        trans.commit();
        session.close();
    	//}
        close();
    }
}
