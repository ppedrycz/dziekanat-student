package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import to2.dziekanat.model.builders.ThesisBuilder;

import javax.persistence.*;
import java.net.URL;
import java.time.LocalDate;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@thesisid")
public class Thesis {
    @Id
    @GeneratedValue
    private long id;

    @Column(nullable = false)
    private String title;

    private LocalDate defenceDate;

    @ManyToOne(optional = false)
    @JoinColumn(name = "submitter")
    private Student submitter;

    @ManyToOne(optional = false)
    @JoinColumn(name = "reviewer")
    private Teacher reviewer;

    @ManyToOne(optional = false)
    @JoinColumn(name = "supervisor")
    private Teacher supervisor;

    private URL thesisUrl;
    private URL reviewUrl;

    public Thesis() {

    }

    public static ThesisBuilder builder() {
        return new ThesisBuilder();
    }

    public Thesis(Student submitter, String title, LocalDate defenceDate, Teacher reviewer, Teacher supervisor, URL thesisUrl, URL reviewUrl) {
        this.defenceDate = defenceDate;
        this.submitter = submitter;
        this.reviewer = reviewer;
        this.supervisor = supervisor;
        this.thesisUrl = thesisUrl;
        this.reviewUrl = reviewUrl;
        this.title = title;
    }

    public LocalDate getDefenceDate() {
        return defenceDate;
    }

    public URL getReviewUrl() {
        return reviewUrl;
    }

    public URL getThesisUrl() {
        return thesisUrl;
    }

    public long getId() {
        return id;
    }

    public Student getSubmitter() {
        return submitter;
    }

    public Teacher getReviewer() {
        return reviewer;
    }

    public Teacher getSupervisor() {
        return supervisor;
    }

    public String getTitle() {
        return title;
    }
}
