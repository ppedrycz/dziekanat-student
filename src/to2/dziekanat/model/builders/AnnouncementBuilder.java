package to2.dziekanat.model.builders;

import to2.dziekanat.model.Announcement;
import to2.dziekanat.model.StudentGroup;
import to2.dziekanat.model.Teacher;

import java.time.LocalDateTime;

public class AnnouncementBuilder {
    private String text;
    private LocalDateTime issueDate = LocalDateTime.now();
    private StudentGroup addressee;
    private Teacher issuer;

    public AnnouncementBuilder withAnnouncementText(String text) {
        this.text = text;
        return this;
    }

    public AnnouncementBuilder issuedOn(LocalDateTime issueDate) {
        this.issueDate = issueDate;
        return this;
    }

    public AnnouncementBuilder addressedTo(StudentGroup addressee) {
        this.addressee = addressee;
        return this;
    }

    public AnnouncementBuilder issuedBy(Teacher issuer) {
        this.issuer = issuer;
        return this;
    }

    private void validateInitializers() {
        boolean hasNoNulls =
                text != null
                        && issueDate != null
                        && addressee != null
                        && issuer != null;

        if (!hasNoNulls) {
            throw new IllegalStateException("Some of the mandatory fields are null");
        }
    }

    public Announcement build() {
        validateInitializers();
        return new Announcement(text, issueDate, addressee, issuer);
    }
}