package to2.dziekanat.model.builders;

import to2.dziekanat.model.ClassesTime;
import to2.dziekanat.model.StudentGroup;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;

public class ClassesTimeBuilder {
    private DayOfWeek dayOfWeek;
    private long weeksBetweenClasses = 1;
    private LocalTime time;
    private Duration duration;
    private StudentGroup studentGroup;

    public ClassesTimeBuilder ofStudentGroup(StudentGroup studentGroup) {
        this.studentGroup = studentGroup;
        return this;
    }

    public ClassesTimeBuilder onDayOfWeek(DayOfWeek dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this;
    }

    public ClassesTimeBuilder withWeeksBetweenClasses(long weeksBetweenClasses) {
        this.weeksBetweenClasses = weeksBetweenClasses;
        return this;
    }

    public ClassesTimeBuilder at(LocalTime time) {
        this.time = time;
        return this;
    }

    public ClassesTimeBuilder lasting(Duration duration) {
        this.duration = duration;
        return this;
    }

    private void validateInitializers() {
        boolean hasNoNulls =
                studentGroup != null
                        && time != null
                        && duration != null
                        && dayOfWeek != null;

        if (!hasNoNulls) {
            throw new IllegalStateException("Some of the mandatory fields are null");
        }
    }

    public ClassesTime build() {
        validateInitializers();
        return new ClassesTime(studentGroup, dayOfWeek, weeksBetweenClasses, time, duration);
    }
}