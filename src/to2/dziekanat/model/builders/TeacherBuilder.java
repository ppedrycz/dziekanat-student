package to2.dziekanat.model.builders;

import to2.dziekanat.model.Teacher;

public class TeacherBuilder {
    private String firstName;
    private String surname;
    private String title;

    public TeacherBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public TeacherBuilder withSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public TeacherBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    private void validateInitializers() {
        if (firstName == null || surname == null || title == null) {
            throw new IllegalStateException("Incomplete data supplied");
        }
    }

    public Teacher build() {
        validateInitializers();
        return new Teacher(firstName, surname, title);
    }
}