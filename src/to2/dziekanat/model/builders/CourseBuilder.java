package to2.dziekanat.model.builders;

import to2.dziekanat.model.Course;
import to2.dziekanat.model.Semester;
import to2.dziekanat.model.Teacher;

public class CourseBuilder {
    private String courseTitle;
    private Semester semester;
    private Teacher supervisor;

    public CourseBuilder withName(String courseName) {
        this.courseTitle = courseName;
        return this;
    }

    public CourseBuilder duringSemester(Semester semester) {
        this.semester = semester;
        return this;
    }

    public CourseBuilder taughtBy(Teacher supervisor) {
        this.supervisor = supervisor;
        return this;
    }

    private void validateInitializers() {
        boolean hasNoNulls =
                courseTitle != null
                        && semester != null
                        && supervisor != null;

        if (!hasNoNulls) {
            throw new IllegalStateException("Some of the mandatory fields are null");
        }
    }

    public Course build() {
        validateInitializers();
        return new Course(courseTitle, semester, supervisor);
    }

}