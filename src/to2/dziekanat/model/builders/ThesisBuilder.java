package to2.dziekanat.model.builders;

import to2.dziekanat.model.Student;
import to2.dziekanat.model.Teacher;
import to2.dziekanat.model.Thesis;

import java.net.URL;
import java.time.LocalDate;

public class ThesisBuilder {
    private LocalDate defenceDate;
    private Student submitter;
    private Teacher reviewer;
    private Teacher supervisor;
    private URL thesisUrl;
    private URL reviewUrl;
    private String title;

    public ThesisBuilder defendedOn(LocalDate defenceDate) {
        this.defenceDate = defenceDate;
        return this;
    }

    public ThesisBuilder defendedBy(Student submitter) {
        this.submitter = submitter;
        return this;
    }

    public ThesisBuilder reviewedBy(Teacher reviewer) {
        this.reviewer = reviewer;
        return this;
    }

    public ThesisBuilder supervisedBy(Teacher supervisor) {
        this.supervisor = supervisor;
        return this;
    }

    public ThesisBuilder thesisUrl(URL thesisUrl) {
        this.thesisUrl = thesisUrl;
        return this;
    }

    public ThesisBuilder reviewUrl(URL reviewUrl) {
        this.reviewUrl = reviewUrl;
        return this;
    }

    public ThesisBuilder withTitle(String title) {
        this.title = title;
        return this;
    }

    private void validateInitializers() {
        if (submitter == null || title == null || defenceDate == null
                || reviewer == null || supervisor == null) {
            throw new IllegalStateException("Incomplete data supplied");
        }
    }

    public Thesis build() {
        validateInitializers();
        return new Thesis(submitter, title, defenceDate, reviewer, supervisor, thesisUrl, reviewUrl);
    }
}