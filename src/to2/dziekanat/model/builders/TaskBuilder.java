package to2.dziekanat.model.builders;

import to2.dziekanat.model.StudentGroup;
import to2.dziekanat.model.Task;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class TaskBuilder {
    private StudentGroup masterStudentGroup;
    private Task masterTask;
    private Set<Task> subtasks = new HashSet<>();
    private LocalDateTime creationTime = LocalDateTime.now();
    private LocalDateTime dueDate;
    private BigDecimal maxScore;
    private BigDecimal scoreMultiplier;
    private String scoreType = "pt";
    private String description;

    public TaskBuilder(StudentGroup masterStudentGroup) {
        this.masterStudentGroup = masterStudentGroup;
    }

    public TaskBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public TaskBuilder withMasterTask(Task masterTask) {
        this.masterTask = masterTask;
        return this;
    }

    public TaskBuilder withSubasks(Set<Task> subtasks) {
        this.subtasks = subtasks;
        return this;
    }

    public TaskBuilder createdOn(LocalDateTime creationTime) {
        this.creationTime = creationTime;
        return this;
    }

    public TaskBuilder withDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public TaskBuilder withMaxScore(BigDecimal maxScore) {
        this.maxScore = maxScore;
        return this;
    }

    public TaskBuilder withScoreMultiplier(BigDecimal scoreMultiplier) {
        this.scoreMultiplier = scoreMultiplier;
        return this;
    }

    public TaskBuilder withScoreType(String scoreType) {
        this.scoreType = scoreType;
        return this;
    }

    private void validatetInitializers() {
        boolean scoresValid = scoreMultiplier != null && maxScore != null;
        scoresValid = scoresValid
                && scoreMultiplier.compareTo(BigDecimal.ZERO) == 1
                && maxScore.compareTo(BigDecimal.ZERO) == 1;

        if (!scoresValid) {
            throw new IllegalStateException("Illegal values for grading supplied");
        }

        boolean hasNoNulls = description != null
                && masterStudentGroup != null
                && creationTime != null
                && dueDate != null
                && scoreType != null
                && subtasks != null;

        if (!hasNoNulls) {
            throw new IllegalStateException("Some of the mandatory attributes are null");
        }
    }

    public Task build() {
        validatetInitializers();
        return new Task(description, masterStudentGroup, creationTime, dueDate, scoreMultiplier, maxScore, scoreType, masterTask, subtasks);
    }
}