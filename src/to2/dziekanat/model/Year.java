package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@yearid")
public class Year {
	@GeneratedValue
	@Id
	private long id;

	@Column(nullable = false)
	private long year;

	@OneToMany(mappedBy = "year")
	@OrderColumn(name = "semesterNumber")
	private List<Semester> semesters = new ArrayList<>();

	@OneToMany(mappedBy = "primaryYear")
	private Set<Student> students = new HashSet<>();

	public Year() {

	}

	public Year(long year) {
		this.year = year;
	}

	public long getId() {
		return id;
	}

	public long getYear() {
		return year;
	}

	public List<Semester> getSemesters() {
		return semesters;
	}

	public Set<Student> getStudents() {
		return students;
	}
}
