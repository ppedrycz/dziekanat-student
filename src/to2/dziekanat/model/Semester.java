package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import to2.dziekanat.model.builders.SemesterBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@semesterid")
public class Semester {
	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private LocalDate startDate;

	@Column(nullable = false)
	private LocalDate endDate;

	@Column(nullable = false)
	private LocalDate finalGradingDate;

	@Column(nullable = false)
	private long semesterNumber;

	@ManyToOne(cascade = CascadeType.MERGE)
	@OrderColumn(name = "semesterNumber")
	private Year year;

	@OneToMany(mappedBy = "semester")
	private Set<Course> courses = new HashSet<>();

	public Semester() {

	}

	public static SemesterBuilder builder() {
		return new SemesterBuilder();
	}

	public Semester(Year year, long semesterNumber, LocalDate startDate, LocalDate endDate, LocalDate finalGradingDate) {
		this.year = year;
		this.startDate = startDate;
		this.endDate = endDate;
		this.finalGradingDate = finalGradingDate;
		this.semesterNumber = semesterNumber;
	}

	public long getSemesterNumber() {
		return semesterNumber;
	}

	public long getId() {
		return id;
	}
	public LocalDate getStartDate() {
		return startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public LocalDate getFinalGradingDate() {
		return finalGradingDate;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public Year getYear() {
		return year;
	}
}
