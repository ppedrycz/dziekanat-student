package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import to2.dziekanat.model.builders.TaskBuilder;
import to2.dziekanat.model.converters.LocalDateTimeDeserializer;
import to2.dziekanat.model.converters.LocalDateTimeSerializer;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@taskid")
public class Task extends Gradeable {
	@Column(nullable = false)
	private String description;

	@ManyToOne(optional = false)
	@JoinColumn(name = "masterStudentGroup")
	private StudentGroup masterStudentGroup;

	@ManyToOne
	@JoinColumn(name = "masterTask", nullable = true)
	private Task masterTask;

	@OneToMany(mappedBy = "masterTask", cascade = CascadeType.REMOVE)
	private Set<Task> subtasks = new HashSet<>();

	@Column(nullable = false)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime creationTime;

	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	private LocalDateTime dueDate;

	public Task() {

	}

	public Task(String description,
				StudentGroup masterStudentGroup,
				LocalDateTime creationTime,
				LocalDateTime dueDate,
				BigDecimal scoreMultiplier,
				BigDecimal maxScore,
				String scoreType,
				Task masterTask,
				Set<Task> subtasks) {
		this.description = description;
		this.masterStudentGroup = masterStudentGroup;
		this.masterTask = masterTask;
		this.subtasks = subtasks;
		this.creationTime = creationTime;
		this.dueDate = dueDate;
		this.scoreMultiplier = scoreMultiplier;
		this.scoreType = scoreType;
		this.maxScore = maxScore;
	}

	public static TaskBuilder builder(StudentGroup masterStudentGroup) {
		return new TaskBuilder(masterStudentGroup);
	}

	public String getDescription() {
		return description;
	}

	public Task getMasterTask() {
		return masterTask;
	}

	public LocalDateTime getCreationTime() {
		return creationTime;
	}

	public LocalDateTime getDueDate() {
		return dueDate;
	}

	public StudentGroup getMasterStudentGroup() {
		return masterStudentGroup;
	}

	public Set<Task> getSubtasks() {
		return subtasks;
	}

	@Override
	public Set<? extends Gradeable> getSubgradables() {
		return subtasks;
	}
}
