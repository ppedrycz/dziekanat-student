package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@coursecomponentid")
public class CourseComponent extends Gradeable {
	private CourseComponentType type;

	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "masterCourse")
	private Course masterCourse;

	@OneToMany(mappedBy = "masterCourseComponent")
	private Set<StudentGroup> studentGroups = new HashSet<>();

	public CourseComponent() {

	}

	public CourseComponent(Course masterCourse, CourseComponentType type) {
		this.masterCourse = masterCourse;
		this.type = type;
	}

	@Override
	public Set<? extends Gradeable> getSubgradables() {
		return studentGroups;
	}

	public CourseComponentType getType() {
		return type;
	}

	public Course getMasterCourse() {
		return masterCourse;
	}

	public Set<StudentGroup> getStudentGroups() {
		return studentGroups;
	}

	public Set<Student> getEnrolledStudents() {
		return studentGroups.stream()
				.flatMap(studentGroup -> studentGroup.getStudents().stream())
				.collect(Collectors.toSet());
	}
}
