package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import to2.dziekanat.model.builders.GradeBuilder;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(uniqueConstraints = {
		@UniqueConstraint(columnNames =
				{"gradedStudent", "gradedSomething"})})
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@gradeid")
public class Grade {
	@Id
	@GeneratedValue
	private long id;

	@Column(nullable = false)
	private BigDecimal score;

	@Column(nullable = false)
	private LocalDateTime gradingTime = LocalDateTime.now();

	@ManyToOne(optional = false)
	@JoinColumn(name = "gradedStudent", nullable = false)
	private Student gradedStudent;

	@ManyToOne(optional = false)
	@JoinColumn(name = "gradedSomething", nullable = false)
	private Gradeable gradedSomething;

	@OneToMany(mappedBy = "commentedGrade")
	private Set<FeedbackMessage> feedbackMessages = new HashSet<>();

	public Grade() {

	}

	public Grade(BigDecimal score,
				 LocalDateTime gradingTime, Student gradedStudent, Gradeable gradedSomething) {
		this.score = score;
		this.gradingTime = gradingTime;
		this.gradedStudent = gradedStudent;
		this.gradedSomething = gradedSomething;
	}

	public static GradeBuilder builder(Student student, Gradeable gradeable) {
		return new GradeBuilder(student, gradeable);
	}

	public BigDecimal getScore() {
		return score;
	}

	public void setScore(BigDecimal score) {
		this.score = score;
	}

	public LocalDateTime getGradingTime() {
		return gradingTime;
	}

	public void setGradingTime(LocalDateTime gradingTime) {
		this.gradingTime = gradingTime;
	}

	public long getId() {
		return id;
	}

	public Student getGradedStudent() {
		return gradedStudent;
	}

	public Gradeable getGradedSomething() {
		return gradedSomething;
	}

	public Set<FeedbackMessage> getFeedbackMessages() {
		return feedbackMessages;
	}
}
