package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import to2.dziekanat.model.builders.StudentBuilder;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@studentid")
public class Student extends Person {

	@Column(nullable = false)
	@Pattern(regexp = "\\d+")
	private String studentIdNumber;

	@OneToMany(mappedBy = "submitter")
	Set<Thesis> defendedTheses = new HashSet<>();

	@ManyToMany
	@JoinTable(joinColumns = {@JoinColumn(name = "studentId", referencedColumnName = "id")},
			inverseJoinColumns = {@JoinColumn(name = "groupId", referencedColumnName = "id")})
	private Set<StudentGroup> attendedGroups = new HashSet<>();

	@ManyToOne(optional = false, cascade = CascadeType.MERGE)
	@JoinColumn(name = "primaryYear")
	private Year primaryYear;

	public String getStudentIdNumber() {
		return studentIdNumber;
	}

	public Set<StudentGroup> getAttendedGroups() {
		return attendedGroups;
	}

	public Set<Thesis> getDefendedTheses() {
		return defendedTheses;
	}

	public Year getPrimaryYear() {
		return primaryYear;
	}

	public Student() {

	}

	public static StudentBuilder builder() {
		return new StudentBuilder();
	}

	public Student(String firstName, String surname, String studentIdNumber, Year primaryYear, Set<Thesis> defendedTheses, Set<StudentGroup> attendedGroups) {
		this.name = firstName;
		this.surname = surname;
		this.studentIdNumber = studentIdNumber;
		this.defendedTheses = defendedTheses;
		this.attendedGroups = attendedGroups;
		this.primaryYear = primaryYear;
	}
}
