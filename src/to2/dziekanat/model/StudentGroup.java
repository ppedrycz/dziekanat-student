package to2.dziekanat.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import to2.dziekanat.model.builders.StudentGroupBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@studengroupid")
public class StudentGroup extends Gradeable {
	@Column(nullable = false)
	private String name;

	@ManyToMany(mappedBy = "attendedGroups")
	private Set<Student> students = new HashSet<>();

	@ManyToOne(optional = false)
	@JoinColumn(name = "groupSupervisor")
	private Teacher groupSupervisor;

	//TODO: document cascading
	@ManyToOne(optional = false, cascade = CascadeType.ALL)
	@JoinColumn(name = "masterCourseComponent")
	private CourseComponent masterCourseComponent;

	@OneToMany(mappedBy = "masterStudentGroup")
	private Set<Task> assignedTasks = new HashSet<>();

	@OneToMany(mappedBy = "addressee")
	@OrderBy("issueDate desc")
	private List<Announcement> announcements = new ArrayList<>();

	@OneToMany(mappedBy = "studentGroup", cascade = CascadeType.ALL)
	private Set<ClassesTime> classTimes = new HashSet<>();

	public StudentGroup() {

	}

	public StudentGroup(String name, CourseComponent masterCourseComponent, Teacher groupSupervisor, Set<ClassesTime> classTimes) {
		this.masterCourseComponent = masterCourseComponent;
		this.groupSupervisor = groupSupervisor;
		this.name = name;
		this.classTimes = classTimes;
	}

	public static StudentGroupBuilder builder() {
		return new StudentGroupBuilder();
	}

	@Override
	public Set<? extends Gradeable> getSubgradables() {
		return assignedTasks;
	}

	public String getName() {
		return name;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public Teacher getGroupSupervisor() {
		return groupSupervisor;
	}

	public CourseComponent getMasterCourseComponent() {
		return masterCourseComponent;
	}

	public Set<Task> getAssignedTasks() {
		return assignedTasks;
	}

	public List<Announcement> getAnnouncements() {
		return announcements;
	}

	public Set<ClassesTime> getClassTimes() {
		return classTimes;
	}

	public Set<Task> getRootTasks() {
		return assignedTasks.stream()
				.filter(task -> task.getMasterTask() == null)
				.collect(Collectors.toSet());
	}
}
