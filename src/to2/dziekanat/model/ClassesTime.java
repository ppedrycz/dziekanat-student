package to2.dziekanat.model;

import to2.dziekanat.model.builders.ClassesTimeBuilder;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;

@Entity
public class ClassesTime {
    @GeneratedValue
    @Id
    private long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "studentGroup")
    private StudentGroup studentGroup;

    private DayOfWeek dayOfWeek;
    private Duration duration;
    private LocalTime time;
    private long weeksBetweenClasses;

    public ClassesTime() {

    }

    public static ClassesTimeBuilder builder() {
        return new ClassesTimeBuilder();
    }

    public ClassesTime(StudentGroup studentGroup, DayOfWeek dayOfWeek, long weeksBetweenClasses, LocalTime time, Duration duration) {
        this.dayOfWeek = dayOfWeek;
        this.time = time;
        this.duration = duration;
        this.weeksBetweenClasses = weeksBetweenClasses;
        this.studentGroup = studentGroup;
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public LocalTime getTime() {
        return time;
    }

    public LocalTime getEndTime() {
        return time.plusMinutes(duration.toMinutes());
    }

    public long getId() {
        return id;
    }

    public Duration getDuration() {
        return duration;
    }

    public long getWeeksBetweenClasses() {
        return weeksBetweenClasses;
    }

    public StudentGroup getStudentGroup() {
        return studentGroup;
    }
}
