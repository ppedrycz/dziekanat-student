package to2.dziekanat.util;

import com.vaadin.ui.Component;

public abstract class AbstractComponentCreator<T extends Component, S extends AbstractComponentCreator> {

    public S width(String width) {
        get().setWidth(width);
        return instance();
    }

    public S height(String height) {
        get().setHeight(height);
        return instance();
    }

    public S sizeFull() {
        get().setSizeFull();
        return instance();
    }

    public S sizeUndefined() {
        get().setSizeUndefined();
        return instance();
    }

    public S heightUndefined() {
        get().setHeightUndefined();
        return instance();
    }

    public S widthUndefined() {
        get().setWidthUndefined();
        return instance();
    }

    public S styles(String... styles) {
        for (String style : styles) {
            get().addStyleName(style);
        }
        return instance();
    }

    public S caption(String caption) {
        get().setCaption(caption);
        return instance();
    }

    public S visible(boolean visible) {
        get().setVisible(visible);
        return instance();
    }

    public abstract T get();

    public abstract S instance();

}
