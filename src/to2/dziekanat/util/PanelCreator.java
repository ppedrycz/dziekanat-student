package to2.dziekanat.util;

import com.vaadin.ui.Panel;

public class PanelCreator extends AbstractComponentCreator<Panel, PanelCreator> {

    private final Panel panel;

    public PanelCreator() {
        panel = new Panel();
    }

    @Override
    public Panel get() {
        return panel;
    }

    @Override
    public PanelCreator instance() {
        return this;
    }
}
