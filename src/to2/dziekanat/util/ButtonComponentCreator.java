package to2.dziekanat.util;

import com.vaadin.ui.Button;

public class ButtonComponentCreator extends AbstractComponentCreator<Button, ButtonComponentCreator> {

    private final Button button;

    public ButtonComponentCreator() {
        this.button = new Button();
    }

    public ButtonComponentCreator onClick(Button.ClickListener listener) {
        button.addClickListener(listener);
        return this;
    }

    @Override
    public Button get() {
        return button;
    }

    @Override
    public ButtonComponentCreator instance() {
        return this;
    }
}
