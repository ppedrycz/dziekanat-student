package to2.dziekanat.util;


import com.vaadin.ui.TextField;

public class TextFieldCreator extends AbstractComponentCreator<TextField, TextFieldCreator> {

    private final TextField textField;

    public TextFieldCreator() {
        textField = new TextField();
    }

    public TextFieldCreator inputprompt(String input) {
        textField.setInputPrompt(input);
        return this;
    }

    public TextFieldCreator value(String value) {
        textField.setValue(value);
        return this;
    }

    public TextFieldCreator readOnly() {
        textField.setReadOnly(true);
        return this;
    }

    public TextFieldCreator enabled(boolean enabled) {
        textField.setEnabled(enabled);
        return this;
    }

    @Override
    public TextField get() {
        return textField;
    }

    @Override
    public TextFieldCreator instance() {
        return this;
    }
}
