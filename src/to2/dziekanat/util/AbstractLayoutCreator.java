package to2.dziekanat.util;

import com.vaadin.event.LayoutEvents;
import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;

public abstract class AbstractLayoutCreator<T extends AbstractOrderedLayout, S extends AbstractLayoutCreator> extends AbstractComponentCreator<T, S> {

    public S addComponent(Component c) {
        get().addComponent(c);
        return instance();
    }

    public S margin() {
        get().setMargin(true);
        return instance();
    }

    public S padding() {
        get().setSpacing(true);
        return instance();
    }

    public S onClick(LayoutEvents.LayoutClickListener listsner) {
        get().addLayoutClickListener(listsner);
        return instance();
    }

    public S align(Component c, Alignment a) {
        get().setComponentAlignment(c, a);
        return instance();
    }
}
