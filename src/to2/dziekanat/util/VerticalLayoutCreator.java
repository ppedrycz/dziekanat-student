package to2.dziekanat.util;

import com.vaadin.ui.VerticalLayout;

public class VerticalLayoutCreator extends AbstractLayoutCreator<VerticalLayout, VerticalLayoutCreator> {

    private final VerticalLayout layout;

    public VerticalLayoutCreator() {
        this.layout = new VerticalLayout();
    }

    @Override
    public VerticalLayout get() {
        return layout;
    }

    @Override
    public VerticalLayoutCreator instance() {
        return this;
    }
}
