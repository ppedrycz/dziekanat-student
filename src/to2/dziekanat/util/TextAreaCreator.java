package to2.dziekanat.util;

import com.vaadin.ui.TextArea;

public class TextAreaCreator extends AbstractComponentCreator<TextArea, TextAreaCreator> {

    private final TextArea textArea;

    public TextAreaCreator() {
        textArea = new TextArea();
    }

    @Override
    public TextArea get() {
        return textArea;
    }

    @Override
    public TextAreaCreator instance() {
        return this;
    }
}
