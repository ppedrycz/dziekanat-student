package to2.dziekanat.util;

import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;

public class ExpandableComponent extends VerticalLayout {
    private final Component component;
    private final Button expandButton;

    public ExpandableComponent(String caption, Component component) {
        this.component = component;
        this.expandButton = GUI.button().caption(caption)
                .onClick(event -> component.setVisible(!component.isVisible())).get();
        addComponent(expandButton);
        addComponent(component);
        component.setVisible(false);
    }

    public void expand() {
        component.setVisible(true);
    }

    public void hide() {
        component.setVisible(false);
    }

}
